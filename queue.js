let collection = [];

// Write the queue functions below.



//1 Method A & B
let print = () => { return collection }

//2-3 Method A
// let enqueue = (data) => { collection.push(data); return collection };

//2-3 Method B
let enqueue = (data) => { collection[collection.length] = data; return collection};

//4-6 Method A
let dequeue = (data) => { collection.shift(data); return collection };

//4-6 Method B
// Array.prototype.delete
// let dequeue = (data) => { collection[shift(data); return collection };

//7
let front = () => { return collection[0]};

//8
let size = () => { return collection.length};

//9
let isEmpty = () => { return collection.length === null ? true : false};

module.exports = { 
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty

};